var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var ctx = document.getElementById('myChart').getContext('2d');


function getAuthorization() {
	return "Basic " + btoa(username + ":" + password);
}

function showData() {
	var ehrId = $("#ehrId3").val().trim();
	var type = $("#dataType").val().trim();
	console.log(type);
	if(!ehrId || !type) {
		document.getElementById("noteG").style.visibility="visible";
		document.getElementById("noteG").innerHTML = "Vnesite Ehr id in izberite tip podatkov!"
		document.getElementById("noteG").className = "noteErr";
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
				"Authorization": getAuthorization()
			},
    	success: function (data) {
  			var party = data.party;
			if (type == "body_temperature") {
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + type,
					type: "GET",
					headers: {
						"Authorization": getAuthorization()
					},
					success: function (res) {
						document.getElementById("noteG").style.visibility="visible";
						document.getElementById("noteG").innerHTML = "Uspešen prikaz podatkov"
						document.getElementById("partyName").innerHTML = "Ime: " + party.firstNames + ", Priimek: " + party.lastNames + ", Datum rojstva: " + party.dateOfBirth; 
						document.getElementById("noteG").className = "note";
						var data = [];
						var time = [];
						var unit = res[0].unit;
						for (var i in res) {
							data.push(res[i].temperature);
							time.push(res[i].time);
						}
						for (var i in data) {
							console.log(time[i]+" - "+data[i])
						}
						drawGraph(unit, data, time);
					},
					error: function (err) {
						console.log(JSON.parse(err.responseText).userMessage);
						document.getElementById("noteG").innerHTML = "Napaka! [" + JSON.parse(err.responseText).userMessage + "]";
						document.getElementById("noteG").className = "noteErr";
						document.getElementById("noteG").style.visibility = "visible";
					}
					
				});
			} else if (type == "weight") {
				console.log("teza");
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + type,
					type: "GET",
					headers: {
						"Authorization": getAuthorization()
					},
					success: function (res) {
						document.getElementById("noteG").style.visibility="visible";
						document.getElementById("noteG").innerHTML = "Uspešen prikaz podatkov"
						document.getElementById("partyName").innerHTML = "Ime: " + party.firstNames + ", Priimek: " + party.lastNames + ", Datum rojstva: " + party.dateOfBirth; 
						document.getElementById("noteG").className = "note";
						var data = [];
						var time = [];
						var unit = res[0].unit;
						for (var i in res) {
							data.push(res[i].weight);
							time.push(res[i].time);
						}
						for (var i in data) {
							console.log(time[i]+" - "+data[i])
						}
						drawGraph(unit, data, time);
					},
					error: function (err) {
						console.log(JSON.parse(err.responseText).userMessage);
						document.getElementById("noteG").innerHTML = "Napaka! [" + JSON.parse(err.responseText).userMessage + "]";
						document.getElementById("noteG").className = "noteErr";
						document.getElementById("noteG").style.visibility = "visible";
					},
				});
			}
  		},
  		error: function(err) {
			document.getElementById("noteG").style.visibility="visible";
  			document.getElementById("noteG").innerHTML = "Napaka! [" + JSON.parse(err.responseText).userMessage + "]";
			document.getElementById("noteG").className = "noteErr";
  		}
		});
	}
}
function drawGraph (units, data, time) {
	for (var i in time) {
		time[i] = time[i].split("T")[0];
	}
	var chart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: time,
			datasets: [{
				label: units,
				borderColor: '#57b2df',
				data: data,
			}]
		},
		options: {
			legend: {
				display: false
			},
			tooltips: {
			
			},
			layout: {
				padding: {
					top: 10,
					bottom: 50,
					left: 50,
					right: 50
				}
			},
		}
	});
	
}

Chart.defaults.global.defaultFontFamily = "'Courier New', Courier, monospace";
Chart.defaults.global.defaultFontSize = 14;


