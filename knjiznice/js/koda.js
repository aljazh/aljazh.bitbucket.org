var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var xmlUrl;


function getDMY() {
	var d = new Date();
	var day = d.getDate();
	var month = d.getMonth()+1;
	day = checkTime(day);
	month = checkTime(month);
	var year = d.getFullYear();
	return (year+"-"+month+"-"+day);
}
function getTime() {
	var d = new Date();
	var h = d.getHours();
	var m = d.getMinutes();
	var s = d.getSeconds();
	h = checkTime(h);
	m = checkTime(m);
	s = checkTime(s);
	return (h + ":" + m + ":" + s);
}

function checkTime(i) {
	if (i < 10) {i = "0" + i};
		return i;
	}
function removeZero(i) {
	if (i < 10) {return i.split("0")[1]};
	return (i);
}



function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}
function createNewEhr() {
	var name = $("#createName").val().trim();
	var surname = $("#createSurn").val().trim();
	var birth = $("#createBirth").val().trim();
	var ehrid = requestNewEhr(name, surname, birth);
}

function requestNewEhr(name, surname, birth) {
	if(!name || !surname || !birth) {
		document.getElementById("noteEhr").style.visibility="visible";
		document.getElementById("noteEhr").className = "noteErr";
		document.getElementById("noteEhr").innerHTML = "Vnesite zahtevane podatke!";
		console.log("false input - createNewEhr()");
	} else {
		$.ajax({
			url: baseUrl + "/ehr",
			type: "POST",
			headers: {
				"Authorization": getAuthorization()
			},
			success: function (data) {
				var ehrId = data.ehrId;
				var partyData = {
					firstNames: name,
					lastNames: surname,
					dateOfBirth: birth,
					additionalInfo: {"ehrId": ehrId}
				};
				$.ajax({
					url: baseUrl + "/demographics/party",
					type: "POST",
					headers: {
						"Authorization": getAuthorization()
					},
					contentType: "application/json",
					data: JSON.stringify(partyData),
					success: function (party) {
						if (party.action = "CREATE") {
							document.getElementById("noteEhr").style.visibility="visible";
							document.getElementById("noteEhr").className = "note";
							document.getElementById("noteEhr").innerHTML = "Uspešno ustvarjen Ehr id: " + ehrId;
							console.log(ehrId);
							document.getElementById("inputPage").innerHTML += "<div>" + name + " " + surname + " - " + birth + " - Ustvarjen Ehr id: " + ehrId + "</div>";
							$("#ehrId").val(ehrId);
							$("#ehrId2").val(ehrId);
							$("#ehrId3").val(ehrId);
							return (ehrId);
						}
					},
					error: function (err) {
						document.getElementById("noteEhr").style.visibility="visible";
						document.getElementById("noteEhr").className = "noteErr"
						document.getElementById("noteEhr").innerHTML = "Napaka! [" + JSON.parse(err.responseText).userMessage+"]";
					}
				});
			}
		})
	}
}			
function getEhrId() {
	var ehrId = $("#ehrId").val().trim();
	
	if(!ehrId) {
		document.getElementById("noteIme").style.visibility="visible";
		document.getElementById("noteIme").innerHTML = "Vnesite Ehr id!"
		document.getElementById("noteIme").className = "noteErr";
		console.log("false input - getEhrId()")
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
				"Authorization": getAuthorization()
			},
    	success: function (data) {
  			var party = data.party;
			document.getElementById("noteIme").style.visibility="visible";
			document.getElementById("noteIme").innerHTML = "Ime: " + party.firstNames + ", Priimek: " + party.lastNames + ", Datum rojstva: " + party.dateOfBirth; 
			document.getElementById("noteIme").className = "note";
			$("#ehrId").val(ehrId);
			$("#ehrId2").val(ehrId);
			$("#ehrId3").val(ehrId);
  		},
  		error: function(err) {
			document.getElementById("noteIme").style.visibility="visible";
  			document.getElementById("noteIme").innerHTML = "Napaka! [" + JSON.parse(err.responseText).userMessage + "]";
			document.getElementById("noteIme").className = "noteErr";
  		}
		});
	}
}
function generirajVse() {
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
	alert("Scroll to see results!");
}
function generirajPodatke(id) {
	var nameTable = ["Luka", "Jaka", "Marko", "Filip", "Simona", "Marjeta", "Mateja", "Anja", "Nika", "Nuša", "Samanta", "Domen", "Anže"]
	var surnameTable = ["Petrič", "Kos", "Rek", "Kovač", "Kotnik", "Virant", "Rozman", "Krebs", "Močilnik", "Smolar"]
	var birth;
	var name;
	var surname;
	//var temperature;
	//var time = (getDMY() + "T" + getTime());
	//var ehrId;
	//var sp;
	//var dp;
	switch(id) {
		case 1:
			name = nameTable[rndInt(0,13)];
			surname = surnameTable[rndInt(0,9)];
			birth = "1953-08-03";
			requestNewEhr(name, surname, birth);
			//ehrId = "bf960804-bb77-479a-a7ef-aaa4d1a7e30e";
			//height = rndInt(177,181);
			//weight = rndInt(56,69);
			//temperature = rndInt(35,38);
			//sp = rndInt(98,111);
			//dp = rndInt(100, 121);
			//writeData(ehrId, time, height, weight, temperature, sp, dp);
			break;
		case 2:
			name = nameTable[rndInt(0,7)];
			surname = surnameTable[rndInt(0,7)];
			birth = "1953-08-03";
			requestNewEhr(name, surname, birth);
			//ehrId = "c3d9bfc8-521a-4b70-864f-6516d635a95b";
			//height = rndInt(183,186);
			//weight = rndInt(66,79);
			//temperature = rndInt(34,37);
			//sp = rndInt(98,111);
			//dp = rndInt(100, 121);
			//writeData(ehrId, time, height, weight, temperature, sp, dp);
			break;
		case 3:
			name = nameTable[rndInt(0,7)];
			surname = surnameTable[rndInt(0,7)];
			birth = "1953-08-03";
			requestNewEhr(name, surname, birth);
			//ehrId = "72b2da06-a0f5-47de-a725-651d9fc418d1";
			//height = rndInt(187,189);
			//weight = rndInt(88,99);
			//temperature = rndInt(35,37);
			//sp = rndInt(98,110);
			//dp = rndInt(100, 110);
			//writeData(ehrId, time, height, weight, temperature, sp, dp);
			break;
		default:
	}
}
function rndInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}
function addData() {
	var ehrId = $("#ehrId2").val().trim();
	if(!ehrId) {
		document.getElementById("noteInput").style.visibility="visible"
		document.getElementById("noteInput").innerHTML = "Vnesite Ehr id!";
		document.getElementById("noteInput").className = "noteErr";
	} else {		
		var height = $("#height").val().trim();
		var weight = $("#weight").val().trim();
		var temperature = $("#temperature").val().trim();
		var pressure = $("#bPressure").val().trim();
		var sPress = pressure.split("/")[0];
		var dPress = pressure.split("/")[1];
		writeData(ehrId, "", height, weight, temperature, sPress, dPress);
	}
}
function writeData(ehrId, time, height, weight, temperature, sPress, dPress) {
	
	time = (getDMY() + "T" + getTime());

		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": time,
		    "vital_signs/height_length/any_event/body_height_length": height,
		    "vital_signs/body_weight/any_event/body_weight": weight,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperature,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sPress,
		    "vital_signs/blood_pressure/any_event/diastolic": dPress
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
			commiter: "default"
		};
		$.ajax({
			url: baseUrl + "/composition?" + $.param(parametriZahteve),
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(podatki),
			headers: {
				"Authorization": getAuthorization()
			},
			success: function (res) {
				document.getElementById("noteInput").style.visibility = "visible";
				document.getElementById("noteInput").className = "note";
				document.getElementById("noteInput").innerHTML = "Podatki so bili uspešno poslani!";
				document.getElementById("clickXml").style.visibility="visible";
				xmlUrl = res;
				console.log(res);
				},
			  error: function(err) {
				document.getElementById("noteInput").innerHTML = "Napaka! [" + JSON.parse(err.responseText).userMessage + "]";
				document.getElementById("noteInput").className = "noteErr";
				document.getElementById("noteInput").style.visibility = "visible";
			  }
		});
	
}

function openXml() {
	window.open(xmlUrl.meta.href);
}
const UKC_LAT = 46.0536077;
const UKC_LNG = 14.5247825;
var mapa;
function createMap() {
	var mapOptions = {
		center: [UKC_LAT, UKC_LNG],
		zoom: 9
	};
	mapa = new L.map("mapa", mapOptions);
	var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
	mapa.addLayer(layer);
}
function showHospital(latLngs) {
	var a;
	console.log((latLngs));
	var polygon = L.polygon(a, {color: 'red'}).addTo(mapa);
}
function getHospitals() {
	$.ajax({
		url: "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json",
		type: "GET",
		success: function(json) {
			for (i in json.features) {
				latLngs = JSON.stringify(json.features[i].geometry.coordinates);
				showHospital(latLngs);
			}
			console.log(json.features[0].geometry.coordinates)
		},
		error: function(err) {
			console.log(err)
		}
	})
}

var latlngs;





// zoom the map to the polygon

//map.fitBounds(polygon.getBounds());









